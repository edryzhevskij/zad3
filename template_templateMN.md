---
author: Edward Ryzewski
title: Sri Lanka - raj na ziemi
subtitle: Beamer present
date: 06.11.2022
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Sri Lanka

![Sri Lanka](sri_lanka.jpeg){ height=50% width=50%}

Państwo w Azji Południowej, na wyspie Cejlon (pod tą nazwą znane do 1972) wraz z mniejszymi przybrzeżnymi wyspami. Oddzielone od Półwyspu Indyjskiego Cieśniną Palk i Zatoką Mannar.

Od wschodu oblewane przez Zatokę Bengalską, od południa otwartym Oceanem Indyjskim. Największe miasta kraju to: Kolombo, Dehiwala, Moratuwa, Dżafna, Kandy, Galle, Kalmunai.

Obecna nazwa jest współczesną adaptacją nazwy występującej w **Ramajanie**, gdzie wyspa znana jest jako *Lanka*.

## Religie

![Sri Lanka](religion.jpeg)

Dane według spisu z 2012:

1. buddyzm – 70,1%
2. hinduizm – 12,6%
3. islam – 9,7%
4. katolicyzm – 6,2%
5. pozostali chrześcijanie – 1,4% (głównie protestantyzm).

Struktura religijna w 2019 roku, według *World Christian Database*:

* buddyzm – 67,8%
* hinduizm – 13,0%,
* chrześcijaństwo – 9,5%
    * katolicy – 7,5%,
    * ewangelikalni (w większości zielonoświątkowcy) – 1%,
    * pozostali (głównie niezależne kościoły, anglikanie i metodyści) – 1%,
* islam – 9,0%,
* brak religii – 0,6%,
* bahaizm – 0,08%,
* nne religie – 0,04%.

## Gospodarka

![Sri Lanka](economy.jpeg){ height=50% width=50%}

Główne źródło utrzymania w **Sri Lance** stanowi rolnictwo. Rolnictwo jest skupione głównie wokół produkcji *herbaty*. 
**Sri Lanka** jest jednym z największych potentatów herbacianych na świecie i z tego stosunkowo małego państwa pochodzi 6,8% światowej produkcji herbaty co jest równe około 330 tysięcy ton (wyspa ta jest czwartym największym producentem herbaty po Chinach, Indiach i Kenii). W państwie tym również rozwija się handel i przemysł, ten ostatni zwłaszcza wokół aglomeracji [Kolombo](https://pl.wikipedia.org/wiki/Kolombo). Jest to jeden z niewielu krajów, w których licznie występują szlachetne kamienie – ze Sri Lanki pochodzi na przykład 400-karatowy szafir [Blue Belle of Asia](https://pl.wikipedia.org/wiki/Blue_Belle_of_Asia).

## Podział administracyjny

Sri Lanka jest podzielona na *prowincje*, które dzielą się na dystrykty.

| **Prowincja**       | **Stolica**       | **Powierzchnia (km2)**  |  **Populacja**  |
| ------------------: | ---------------:  | ------------------:     | -----------:    |
| Centralna           | Kandy             | 5674                    |   2 423 966     |
| Południowa          | Galle             | 5559                    |   2 278 271     |
| Północna            | Dżafna            | 8884                    |   1 311 776     |
| Północno-Centralna  | Anuradhapura      | 10 714                  |   1 104 664     |
| Północno-Zachodnia  | Kurunegala        | 7812                    |   2 169 892     |
| Sabaragamuwa        | Ratnapura         | 4902                    |   1 801 331     |
| Uva                 | Badulla           | 8488                    |   1 177 358     |
| Wschodnia           | Trikunamalaja     | 9996                    |   1 460 939     |
| Zachodnia           | Kolombo           | 3709                    |   5 361 200     |
## Więcej obrazków na dobry humor

![](elef.jpeg){ height=80% width=80%}

![](peak.jpeg){ height=50% width=50%}

![](railway.jpeg){ height=50% width=50%}

<center>KONIEC!</center>